package trainer;

import trainer.Trainer;

public class Action {
    public enum Action_Event {
        ATTACK,
        SWAP,
        ITEM,
    }

    private Action_Event act;
    private int act_num;
    private int itemTarget_num;
    private Trainer trainer, otherTrainer;


    /// @description Attack Action
    public Action(int mov_num, Trainer trainer, Trainer otherTrainer) {
        this.act = Action_Event.ATTACK;
        this.act_num = mov_num;
        this.trainer = trainer;
        this.otherTrainer = otherTrainer;
        this.itemTarget_num = -1;
    }

    /// @description Swap Action
    public Action(int bag_num, Trainer trainer) {
        this.act = Action_Event.SWAP;
        this.act_num = bag_num;
        this.trainer = trainer;
        this.otherTrainer = null;
        this.itemTarget_num = -1;
    }

    /// @description Item Action
    Action(int bag_num, int itemTarget_num, Trainer trainer) {
        this.act = Action_Event.ITEM;
        this.act_num = bag_num;
        this.trainer = trainer;
        this.otherTrainer = null;
        this.itemTarget_num = itemTarget_num;
    }

    public Action_Event getAction() {
        return act;
    }

    public int getActionNumber() {
        return act_num;
    }

    public int getItemTargetNumber() {
        return itemTarget_num;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public Trainer getOtherTrainer() {
        return otherTrainer;
    }
}
