package trainer;

import bagimal.Bagimal;

import java.util.Random;

public class GenericTrainer extends Trainer {

    Random numPicker = new Random();
    private int currentBagimal_num;

    public GenericTrainer(String name, Bagimal firstBagimal) {
        super(name, firstBagimal);
        currentBagimal_num = 0;
    }

    @Override
    public void forceSwap() {
        currentBagimal_num++;
        swapBagimal(currentBagimal_num);
    }

    @Override
    public Action action(Trainer enemy) {
        int action_num = numPicker.nextInt(this.getCurrentBagimal().getMoveCount());
        return new Action(action_num, this, enemy);

    }
}
