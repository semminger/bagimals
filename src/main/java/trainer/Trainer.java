package trainer;

import output.OutputController;
import bagimal.Bagimal;
import item.Item;


import java.util.ArrayList;
import java.util.List;

public abstract class Trainer {

    public static final int MAX_BAGIMALS = 3;
    private String name;
    private Bagimal currentBagimal;
    protected ArrayList<Bagimal> bagimals;
    protected ArrayList<Item> items;


    public Trainer(String name, Bagimal firstBagimal) {
        this.name = name;
        currentBagimal = firstBagimal;
        bagimals = new ArrayList<>();
        bagimals.add(currentBagimal);
        items = new ArrayList<>();
    }

    public Trainer(String name, List<Bagimal> bagimals) {
        this(name, bagimals.get(0));
        bagimals.remove(0);
        for (Bagimal bagimal:
             bagimals) {
            this.addBagimal(bagimal);
        }
    }

    public String getName() {
        return name;
    }

    public void addBagimal(Bagimal newBagimal) {
        if(bagimals.size() < MAX_BAGIMALS) {
            bagimals.add(newBagimal);
        }
    }

    public int getTeamSize() {
        return bagimals.size();
    }

    public Bagimal getBagimal(int bag_num) {
        return bagimals.get(bag_num);
    }

    public void swapBagimal(int bagimal_num) {
        if(bagimal_num >= 0 && bagimal_num < bagimals.size()) {
            Bagimal swapBagimal = bagimals.get(bagimal_num);
            if(swapBagimal != null) {
                // TODO: Check if alive or something
                currentBagimal.swapClear();
                currentBagimal = swapBagimal;
            }
        }
    }

    public String attack(int atk_num, Trainer target) {
        return currentBagimal.attack(atk_num, target.getCurrentBagimal());
    }

    public abstract void forceSwap();

    public Bagimal getCurrentBagimal() {
        return currentBagimal;
    }

    public boolean isFightable() {
        for (Bagimal bagimal:
             bagimals) {
            if(bagimal.isAlive())
                return true;
        }
        return false;
    }

    public void giveItem(Item newItem) {
        items.add(newItem);
    }

    public void useItem(int item_num, int bag_num) {

        // Check if the item_num is in range
        if(item_num < items.size() && item_num >= 0) {

            if(bag_num < bagimals.size() && bag_num >= 0) {
                Bagimal targetBagimal = bagimals.get(bag_num);
                Item targetItem = items.get(item_num);

                OutputController.write(name + " uses a " + targetItem.getName() + " on " + targetBagimal.getName());

                // Use item
                targetItem.use(targetBagimal);

                // Dispose of it
                items.remove(item_num);
            }

        }
    }

    public String executeTeamStatusEffects() {
        StringBuilder output = new StringBuilder();
        for (Bagimal bagimal:
             bagimals) {
            output.append(bagimal.executeStatusEffects());
        }
        return output.toString();
    }

    public abstract Action action(Trainer enemy);

}
