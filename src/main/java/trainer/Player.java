package trainer;

import output.OutputController;
import bagimal.Bagimal;
import item.Item;

import java.util.List;
import java.util.Scanner;

public class Player extends Trainer {

    private Scanner input = new Scanner(System.in);

    public Player(String name, Bagimal firstBagimal) {
        super(name, firstBagimal);
    }

    public Player(String name, List<Bagimal> bagimals) {
        super(name, bagimals);
    }

    @Override
    public void forceSwap() {
        int swap_choice;

        OutputController.write("Choose another Bagimal:");
        while(true) {
            this.printBagimalList();
            OutputController.write("0: GO BACK");
            System.out.print("Choose: ");
            swap_choice = input.nextInt();

            if(swap_choice < 0 || swap_choice > this.bagimals.size()) {
                OutputController.write("This move doesn't exists!");
                continue;
            }

            if(!bagimals.get(swap_choice - 1).isAlive()) {
                OutputController.write("This Bagimal is not able to fight\n");
                continue;
            }

            if(swap_choice != 0) {
                swapBagimal(swap_choice - 1);
                break;
            }
        }
    }

    @Override
    public Action action(Trainer enemy) {
        int action_choice, act_num_choice;

        while(true) {
            printMenu();
            System.out.print("Choose: ");
            action_choice = input.nextInt();

            switch(action_choice) {
                //Attack
                case 1:
                    this.getCurrentBagimal().printMovesList();
                    OutputController.write("0: GO BACK\n");
                    System.out.print("Choose: ");
                    act_num_choice = input.nextInt();

                    if(act_num_choice < 0 || act_num_choice > this.getCurrentBagimal().getMoveCount()) {
                        OutputController.write("This move doesn't exists!");
                        break;
                    }

                    if(act_num_choice != 0)
                        return new Action(act_num_choice-1, this, enemy);

                    break;

                case 2:
                    this.printBagimalList();
                    OutputController.write("0: GO BACK");
                    System.out.print("Choose: ");
                    act_num_choice = input.nextInt();

                    if(act_num_choice == 0)
                        break;

                    if(act_num_choice < 0 || act_num_choice > this.bagimals.size()) {
                        OutputController.write("This Bagimal doesn't exists!");
                        break;
                    }

                    Bagimal choosenBagimal = bagimals.get(act_num_choice-1);


                    // Ask for swapping or Status
                    System.out.print(choosenBagimal.getName() + ":\n"+
                                      "1: INFO\t2: SWAP\t3: ITEM\n" +
                                      "0: GO BACK\n"+
                                      "Choose:");


                    int bagimal_action_choice = input.nextInt();

                    switch(bagimal_action_choice) {

                        // GO BACK
                        case 0:
                            break;

                        // Print the status
                        case 1:
                            choosenBagimal.printStats();
                            break;

                        // Swap the Bagimal if possible
                        case 2:
                            if(choosenBagimal.isAlive()) {
                                return new Action(act_num_choice-1, this);
                            } else {
                                OutputController.write("This Bagimal is not able to fight\n");
                            }
                            break;
                        case 3:
                            printItemList();
                            System.out.print("Choose a item:");

                            int item_num = input.nextInt();

                            if(item_num > 0 && item_num <= items.size()) {
                                return new Action(act_num_choice-1, item_num-1, this);
                            }

                            break;
                        default:
                            OutputController.write("WRONG INPUT");
                    }


            }

        }

    }

    private void printBagimalList() {
        OutputController.write("BAGIMALS: \n");

        int i = 1;
        for (Bagimal bagimal:
             bagimals) {
            System.out.print(i + ": " + bagimal.getName() + " - " + bagimal.statusEffectString());

            if(!bagimal.isAlive())
                System.out.print(" - UNCONSCIOUS\n");
            else
                System.out.print("\n");

            i++;
        }
    }

    private void printItemList() {
        OutputController.write("ITEMS: \n");

        int i = 1;
        for(Item item:
            items) {

            OutputController.write(i + ": " + item.getName());
            i++;
        }
    }

    private void printMenu() {
        OutputController.write("What to do?");
        OutputController.write("1: ATTACK\t2: TEAM");
    }
}
