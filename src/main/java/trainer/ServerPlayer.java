package trainer;

import bagimal.Bagimal;
import bagimal.Bagimal.Status;
import networking.PlayerHandler;
import networking.Room;
import networking.ServerI;

import java.io.IOException;
import java.net.Socket;
import java.util.List;

public class ServerPlayer extends Trainer {
    PlayerHandler pHandler;
    ServerI server;
    Room room = null;
    Trainer enemy;  // To get the enemy for the Action... TODO: Maybe change the Action...

    public ServerPlayer(String name, List<Bagimal> bagimals, Socket playerSocket, ServerI server) {
        super(name, bagimals);
        pHandler = new PlayerHandler(playerSocket, this);
        this.server = server;
    }


    @Override
    public void forceSwap() {
        pHandler.writeToPlayer("REQUEST_FORCE_SWAP;");
    }

    public void newPlayerUpdate(String newMsg) throws IOException {
        System.out.println(getName() + ": " + newMsg);
        // Determine Action
        Action action;
        String[] params = newMsg.split(";");
        switch (params[0]) {
            case "ATTACK":
                action = new Action(Integer.parseInt(params[1]), this, enemy);
                if(room != null)
                    room.retrieveAction(action);
                break;

            case "SWAP":
                action = new Action(Integer.parseInt(params[1]), this);
                if(room != null)
                    room.retrieveAction(action);
                break;

            case "FORCE_SWAP":
                swapBagimal(Integer.parseInt(params[1]));
                if(room != null)
                    room.gotForceSwapUpdate(this);
                break;

            case "DISCONNECT":
                if(room != null)
                    room.playerDisconnected(this);
                else {
                    try {
                        pHandler.closeConnection();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case "ROOM":
                server.gotRoomSelection(this, Integer.parseInt(params[1]));
                break;
            }

    }

    @Override
    public Action action(Trainer enemy) {
        this.enemy = enemy;
        pHandler.writeToPlayer("REQUEST_ACTION;");
        return null;
    }

    public void updatePlayer(Trainer enemy) {
        StringBuilder update = new StringBuilder("UPDATE_SELF;" + getCurrentBagimal().getHp() + ";");
        Status currentStatus = getCurrentBagimal().getCurrentNonVolatileStatus();

        if(currentStatus != null) {
            update.append(currentStatus);
        } else {
            update.append("NONE");
        }
        pHandler.writeToPlayer(update.toString());

        Bagimal enemyBagimal = enemy.getCurrentBagimal();

        int enemyHpPercentage = (int) Math.ceil((enemyBagimal.getHp()*100)/((double)enemyBagimal.getMax_hp()));
        update = new StringBuilder("UPDATE_ENEMY;"+ enemyBagimal.getName() +";"+ enemyHpPercentage + ";");
        currentStatus = enemyBagimal.getCurrentNonVolatileStatus();

        if(currentStatus != null) {
            update.append(currentStatus);
        } else {
            update.append("NONE");
        }
        pHandler.writeToPlayer(update.toString());
    }

    public void sendEndResult(String endResult) {
        pHandler.writeToPlayer("GAME_OVER;" + endResult);
    }

    public void sendOpponentName(String name) {
        pHandler.writeToPlayer("ENEMY_NAME;" + name);
    }

    public void writeToPlayer(String message) {
        pHandler.writeToPlayer(message);
    }

    public void closeConnection() throws IOException {
        pHandler.closeConnection();
    }

    public void setRoom(Room room) {
        if(this.room == null)
            this.room = room;
    }


}
