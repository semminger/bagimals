import bagimal.Bagimal;
import bagimal.BagimalFactory;
import controller.GameController;
import item.Burn_Heal;
import output.OutputController;
import output.OutputType;
import trainer.Action;
import trainer.GenericTrainer;
import trainer.Player;
import trainer.Trainer;

import java.io.FileNotFoundException;
import java.util.List;

public class Game extends GameController {

    List<Action> actions;

    public Game() {
        OutputController.setOutputType(OutputType.CONSOLE);
    }

    public static void main(String[] args) {


        Bagimal my_bagimal1 = null;
        Bagimal my_bagimal2 = null;
        Bagimal enemy_bagimal = null;

        try {
            my_bagimal1 = BagimalFactory.createBagimal("Splash", null, null, 10);
            my_bagimal2 = BagimalFactory.createBagimal("Flamly", null, null, 7);
            enemy_bagimal = BagimalFactory.createBagimal("Flamly", null, null, 8);
        } catch (FileNotFoundException e) {
            System.out.println("File not found\n");
        }




        Player player = new Player("Stefan", my_bagimal2);
        player.addBagimal(my_bagimal1);
        player.giveItem(new Burn_Heal());

        Trainer enemy = new GenericTrainer("Jeff", enemy_bagimal);
        try {
            enemy.addBagimal(BagimalFactory.createBagimal("Splash", null, null, 8));
            enemy.addBagimal(BagimalFactory.createBagimal("Splash", null, null, 8));
            enemy.addBagimal(BagimalFactory.createBagimal("Splash", null, null, 9));
        } catch (FileNotFoundException e) {
            System.out.println("File not found\n");
        }
        new Game().startFight(player, enemy);
    }

    @Override
    public void requestAction() {
        Action trainer1Action, trainer2Action;
        Trainer trainer1 = trainers.get(0);
        Trainer trainer2 = trainers.get(1);
        OutputController.write("YOU:\n");
        trainers.get(0).getCurrentBagimal().printStatus();

        OutputController.write(trainers.get(1).getName() +":\n");
        trainers.get(1).getCurrentBagimal().printStatus();

        if(trainer1.getCurrentBagimal().isCharging()) {
            trainer1Action = new Action(trainer1.getCurrentBagimal().getChargeMoveNumber(), trainer1, trainer2);
        } else {
            trainer1Action = trainer1.action(trainer2);
        }
        if(trainer2.getCurrentBagimal().isCharging()) {
            trainer2Action = new Action(trainer2.getCurrentBagimal().getChargeMoveNumber(), trainer2, trainer1);
        } else {
            trainer2Action = trainer2.action(trainer1);
        }

        actions = determineQue(trainer1Action, trainer2Action);
    }

    @Override
    protected void requestForceSwap(Trainer trainer) {
        if(trainer.isFightable()) {
            trainer.forceSwap();
            OutputController.write(trainer.getName() + " puts " + trainer.getCurrentBagimal().getName() + " into the fight");
            requestAction();
        } else {
            endFight();
        }
    }

    @Override
    protected void endFight() {
        if(trainers.get(0).isFightable()) {
            OutputController.write("YOU WON");
        } else {
            OutputController.write("YOU LOSE");
        }
    }

    @Override
    public void startFight(Trainer trainer1, Trainer trainer2) {
        super.startFight(trainer1, trainer2);
        while(fightNotOver()) {
            requestAction();
            executeRound(actions);
        }
    }
}
