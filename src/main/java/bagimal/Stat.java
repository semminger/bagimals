package bagimal;

public enum Stat {
    ATK,
    DEF,
    SP_ATK,
    SP_DEF,
    SPD
}
