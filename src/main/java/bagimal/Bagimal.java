package bagimal;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Random;

import output.OutputController;
import moves.*;
import type.Type;

public class Bagimal {

    public enum Status {
        FLINCHED(false),   // attack gets skipped for one round
        PARALYZED(true),  // attack gets maybe skipped, speed gets decreased. Needs item
        POISONED(true),   // deals damage at end of the round, heals with item
        SLEEPING(true),   // can't attack. wakes up random or with item
        BURNING(true),     // deals damage at end of round, decreases atk. heals with item
        CONFUSED(false),
        CHARGING(false),
        RECHARGING(false);

        private final boolean isNonVolatile;

        Status(boolean isNonVolatile) {
            this.isNonVolatile = isNonVolatile;
        }

        public boolean isNonVolatile() {
            return isNonVolatile;
        }
    }

    private final int base_hp;
    private int max_hp, hp;
    private EnumMap<Stat, Integer> base_stats;
    private EnumMap<Stat, Integer> stats;
    private int hp_iv;
    private EnumMap<Stat, Integer> ivs;
    private EnumMap<StageStat, Integer> buffs;
    private int lvl;
    private final Type type;
    private final String name, description;
    private boolean alive;

    private ChargeMove chargingMove = null;

    private int rounds_asleep;
    private int rounds_confused;

    public static final int MAX_MOVES = 4;
    public static final int MAX_BUF = 6;
    private static final int MAX_SLEEPING_ROUNDS = 4;
    private static final int MIN_CONFUSED_ROUNDS = 2;
    private static final int MAX_CONFUSED_ROUNDS = 5;

    protected ArrayList<Move> moves;
    protected ArrayList<Status> statuses;

    private Random rand = new Random();

    public Bagimal(int base_hp, EnumMap<Stat, Integer> base_stats, Integer hp_iv, EnumMap<Stat, Integer> ivs, Type type, int lvl,  String name, String description) {

        // General init
        this.base_stats = new EnumMap<>(Stat.class);
        for (Stat stat:
             Stat.values()) {
            this.base_stats.put(stat, base_stats.get(stat));
        }

        this.ivs = new EnumMap<>(Stat.class);
        if(ivs == null) {
            for (Stat stat :
                    Stat.values()) {
                this.ivs.put(stat, rand.nextInt(32));
            }
        } else {
            for (Stat stat :
                    Stat.values()) {
                this.ivs.put(stat, base_stats.get(stat));
            }
        }
        if(hp_iv == null) {
            this.hp_iv = rand.nextInt(32);
        } else {
            this.hp_iv = hp_iv;
        }

        this.base_hp = base_hp;
        this.type = type;
        this.name = name;
        this.description = description;

        // Stat init
        this.max_hp = (int) Math.ceil(((this.base_hp * 2 + this.hp_iv) * lvl) / 100.) + lvl + 10;
        this.hp = this.max_hp;

        stats = new EnumMap<>(Stat.class);
        for (Stat stat:
             Stat.values()) {
            int stat_value = (int) Math.ceil(((base_stats.get(stat) * 2 + this.ivs.get(stat)) * lvl) / 100.) + 5;
            stats.put(stat, stat_value);
        }

        // Status init
        buffs = new EnumMap<>(StageStat.class);
        for (StageStat stat:
             StageStat.values()) {
            buffs.put(stat, 0);
        }

        this.lvl = lvl;

        alive = true;
        rounds_asleep = 0;

        moves = new ArrayList<>();
        statuses = new ArrayList<>();
    }

    public Bagimal(int base_hp, EnumMap<Stat, Integer> base_stats, Integer hp_iv, EnumMap<Stat, Integer> ivs, Type type, int lvl,  String name, String description, List<Move> allMoves) {
        this(base_hp, base_stats, hp_iv, ivs, type, lvl, name, description);
        moves.addAll(allMoves);
    }


    public int getStatValue(Stat stat) {
        return stats.get(stat);
    }

    public int getStatIV(Stat stat) {
        return ivs.get(stat);
    }

    public int getHpIV() {
        return hp_iv;
    }

    public int getBaseStatValue(Stat stat) {
        return base_stats.get(stat);
    }

    public Type getType(){
        return type;
    }

    public String getName() {
        return name;
    }

    public int getMax_hp() {
        return max_hp;
    }

    public int getHp() {
        return hp;
    }

    public Move getMove(int move_num) {
        if (move_num >= 0 && move_num < MAX_MOVES) {
            return moves.get(move_num);
        }

        return null;
    }

    public int getMoveCount() {
        return moves.size();
    }

    public String attack(int atk_num, Bagimal enemy) {
        String output = "";
        if(alive) {
            if (atk_num >= 0 && atk_num < MAX_MOVES) {

                // FLINCHING
                if(statuses.contains(Status.FLINCHED)) {
                    output += name + " flinched.\n";
                    return output;
                }

                if(statuses.contains(Status.PARALYZED)) {
                    output += name + " is paralyzed.\n";

                    // Check if Bagimal loses attack (25% chance)
                    if(rand.nextInt(4) == 0) {
                        output += "It can't attack.\n";
                        return output;
                    }
                }


                if(statuses.contains(Status.SLEEPING)) {
                    rounds_asleep--;

                    if(rounds_asleep == 0) {
                        output += removeStatus(Status.SLEEPING);
                    } else {
                        output += name + "is still sleeping.\n";
                        return output;
                    }
                }

                if(statuses.contains(Status.CONFUSED)) {

                    if(rounds_confused == 0) {
                        output += removeStatus(Status.CONFUSED);
                    } else {
                        output += name + " is confused.\n";

                        // Check if Bagimal attacks its self (50% chance)
                        if(rand.nextInt(2) == 0) {
                            output += "It attacked itself!\n";
                            int damage = (int) ((40 * stats.get(Stat.ATK))/(double)stats.get(Stat.DEF));
                            output += reduceHp(damage);
                            return output;
                        }
                    }
                }

                Move mov = getMove(atk_num);
                //OutputController.write();(name + " uses " + mov.getName());
                output += mov.use(this, enemy);
            }
        } else {
            output += "This Bagimal is unconscious\n";
        }

        return  output;
    }

    public String reduceHp(int hpAmount) {
        String output = "";
        if (hpAmount < hp) {
            hp -= hpAmount;
        } else {
            hp = 0;
            alive = false;
            removeAllStatuses();
            output += name + " died.\n";
        }

        return output;
    }

    public void regenerateHp(int hpAmount) {
        if(hpAmount < max_hp-hp) {
            hp += hpAmount;
        } else {
            hp = max_hp;
        }
    }

    public String giveStatus(Status status) {

        String output = "";

        if(statuses.contains(status)) {
            output += name + " is already " + status + ".\n";
            return output;
        }

        // Check if the status is a non-volatile
        if(status.isNonVolatile()) {

            // Check if the Bagimal doesn't already has a non-volatile status
            if(statuses.stream().noneMatch(s->s.isNonVolatile)) {

                statuses.add(status);
                if(status == Status.SLEEPING) {
                    rounds_asleep = rand.nextInt(MAX_SLEEPING_ROUNDS);
                }
                output += name + " is now " + status + ".\n";
            }
        } else {
            // When it gets flinched, there is no output needed
            if(status == Status.CONFUSED) {
                output += name + " is confused.\n";
                rounds_confused = rand.nextInt(MAX_CONFUSED_ROUNDS + MIN_CONFUSED_ROUNDS) + MIN_CONFUSED_ROUNDS;
            }
            statuses.add(status);
        }

        return output;
    }

    public String removeStatus(Status status) {
        if(statuses.contains(status)) {
            statuses.remove(status);
            return name + " is no longer " + status + ".\n";
        }
        return "";
    }

    public void removeAllStatuses() {
        statuses.clear();
    }

    public String executeStatusEffects() {
        String output = "";
        if(hasStatus(Status.BURNING)) {
            output += name + " took damage from burning\n";
            reduceHp(max_hp/16);
        }

        if(hasStatus(Status.POISONED)) {
            output += name + " took damage from poison\n";
            output += reduceHp(max_hp/16);
        }

        // If the bagimal didn't flinch the remove it at the end of the turn
        if(hasStatus(Status.FLINCHED)) {
            statuses.remove(Status.FLINCHED);
        }

        if(hasStatus(Status.CONFUSED)) {
            rounds_confused--;
        }

        return output;
    }

    public boolean hasStatus(Status status) {
        return statuses.contains(status);
    }

    public String statusEffectString(){

        // There can only be one Status Effect that can be displayed
        for (Status status:
             statuses) {
            if(status != Status.FLINCHED) {
                return status.toString();
            }
        }
        return "";
    }

    // BUFFS

    public String addStatBuff(StageStat stat, int amount) {
        int currentBuff = buffs.get(stat);
        String output = "";
        if(amount > 0) {
            if(currentBuff == MAX_BUF) {
                output += name + "'s "+stat+" can't be increased higher!\n";
            } else {
                buffs.put(stat, Math.min(amount + currentBuff, MAX_BUF));
                output += name + "'s "+stat+" got increased!\n";
            }
        } else  {
            if(currentBuff == -MAX_BUF) {
                output += name + "'s "+stat+" can't be decreased lower!\n";
            } else {
                buffs.put(stat, Math.max(amount + currentBuff, -MAX_BUF));
                output += name + "'s "+stat+" got decreased!\n";
            }
        }
        return output;
    }

    public void swapClear() {
        resetBuffs();
        if(hasStatus(Status.CONFUSED))
            statuses.remove(Status.CONFUSED);
    }

    public void resetBuffs() {
        for (StageStat stat:
             StageStat.values()) {
            buffs.put(stat, 0);
        }
    }

    public int getBuff(StageStat stat) {
        return buffs.get(stat);
    }

    public int getLvl() {
        return lvl;
    }

    public Status getCurrentNonVolatileStatus() {
        for(Status status: statuses) {
            if(status.isNonVolatile) {
                return status;
            }
        }
        return null;
    }

    public int getMovePP(int mov_num) {
        return moves.get(mov_num).getPP();
    }

    public void reduceMovePP(int move_num) {
        moves.get(move_num).reducePP();
    }

    // DEPRECATED
    /*
    public int getTotalPoints() {
        return max_hp + atk + def + spd;
    }
    */


    public void printStats() {
        OutputController.write("Name: " + name);
        OutputController.write("LVL: " + lvl);
        OutputController.write("HP: " + hp + "/" + max_hp);
        OutputController.write("ATK: " + stats.get(Stat.ATK));
        OutputController.write("DEF: " + stats.get(Stat.DEF));
        OutputController.write("SP_ATK: " + stats.get(Stat.SP_ATK));
        OutputController.write("SP_DEF: " + stats.get(Stat.SP_DEF));
        OutputController.write("SPD: " + stats.get(Stat.SPD));
    }



    public void printMoves() {
        OutputController.write("MOVES:\n");

        for (Move mov :
                moves) {
            mov.getInfo();
        }

    }

    public void printStatus() {
        System.out.print(name + ": " +hp +"/" +max_hp +"HP  LVL " + lvl + " " + statusEffectString() + "\n");

        System.out.print("\t");

        int hp_percentage = (int) Math.ceil((hp/(double)max_hp) * 10);

        for(int i = 0; i < 10; i++) {
            if(i < hp_percentage)
                System.out.print("#");
            else
                System.out.print("_");
        }
        OutputController.write("\n");
    }

    public void printMovesList() {
        OutputController.write("MOVES:\n");

        int i = 1;

        for (Move mov :
                moves) {
            OutputController.write(i + ": " + mov.getName());
            i++;
        }
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isCharging() {
        return chargingMove != null;
    }

    public ChargeMove getChargingMove() {
        return chargingMove;
    }

    public void setChargingMove(ChargeMove chargingMove) {
        // Should not be needed but better safe then sry
        if(moves.contains(chargingMove))
            this.chargingMove = chargingMove;
    }

    public void removeChargingMove() {
        this.chargingMove = null;
    }
    // TODO: This is shit maybe change the way attacks are given late... (not by index but by object)
    public int getChargeMoveNumber() {
        return moves.indexOf(chargingMove);
    }

}