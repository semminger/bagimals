package bagimal;

public enum StageStat {
    ATK,
    DEF,
    SP_ATK,
    SP_DEF,
    SPD,
    ACC,
    EVA
}
