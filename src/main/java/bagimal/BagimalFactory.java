package bagimal;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Scanner;

import moves.*;
import type.Type;
public class BagimalFactory {

    public static Bagimal createBagimal(String bagimalFileName, Integer hp_iv, EnumMap<Stat, Integer> ivs, int lvl) throws FileNotFoundException {

        //File bagimalFile = new File("bagimals/" + bagimalFileName + ".bag");
        InputStream bagimalStream = BagimalFactory.class.getResourceAsStream("/bagimals/" + bagimalFileName + ".bag");
        Scanner fileReader = new Scanner(bagimalStream);
        String name = "", desc = "";
        int base_hp = 0;
        EnumMap<Stat, Integer> stats = new EnumMap<>(Stat.class);
        Type type = Type.NORMAL;
        List<Move> moves = new ArrayList<>();

        while(fileReader.hasNextLine()) {
            String line = fileReader.nextLine();
            switch(line) {
                case "[name]":
                    name = fileReader.nextLine();
                    break;

                case "[base_hp]":
                    base_hp = fileReader.nextInt();
                    break;

                case "[base_atk]":
                    stats.put(Stat.ATK,fileReader.nextInt());
                    break;

                case "[base_def]":
                    stats.put(Stat.DEF,fileReader.nextInt());
                    break;

                case "[base_sp_atk]":
                    stats.put(Stat.SP_ATK,fileReader.nextInt());
                    break;

                case "[base_sp_def]":
                    stats.put(Stat.SP_DEF,fileReader.nextInt());
                    break;

                case "[base_spd]":
                    stats.put(Stat.SPD,fileReader.nextInt());
                    break;

                case "[type]":
                    type = Type.valueOf(fileReader.nextLine());
                    break;

                case "[desc]":
                    desc = fileReader.nextLine() + "\n";
                    break;

                case "[moves]":
                    String[] allMoveNames = fileReader.nextLine().split(",");
                    for (String moveName:
                         allMoveNames) {
                        moves.add(MoveFactory.createMove(moveName.trim()));
                    }
                    break;
            }
        }

        return new Bagimal(base_hp, stats, hp_iv, ivs, type, lvl, name, desc, moves);
    }
    //DEPRECATED
    /*
    private static Move getMoveFromString(String moveName) {
        switch(moveName) {
            case "Tackle":
                return Tackle.getMove();

            case "Firecannon":
                return Ember.getMove();

            case "Watergun":
                return Watergun.getMove();

            case "Growl":
                return Growl.getMove();

            case "Harden":
                return Harden.getMove();

            case "Howl":
                return Howl.getMove();

            case "Tail_Whip":
                return Tail_Whip.getMove();

            case "Wing_Attack":
                return Wing_Attack.getMove();

            default:
                return null;
        }
    }

    */

}
