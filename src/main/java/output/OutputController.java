package output;

import networking.ServerI;

public class OutputController {
    private static OutputType oType = OutputType.CONSOLE;
    private static ServerI server = null;


    public static void setOutputType(OutputType newOType) {
        oType = newOType;
    }

    public static void setServer(ServerI newServer) {
        server = newServer;
    }

    public static void write(String message) {
        switch(oType) {
            case CONSOLE:
                System.out.println(message);
                break;

            case NETWORK:
                //server.broadcastMessage(message);
                break;
        }
    }
}
