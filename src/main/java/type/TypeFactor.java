package type;

import moves.MoveFactory;

import java.io.InputStream;
import java.util.EnumMap;
import java.util.Locale;
import java.util.Scanner;

public class TypeFactor {
    private static EnumMap<Type, EnumMap<Type, Double>> typeFactors = null;

    public static double getTypeFactor(Type attackerType, Type defenderType) {
        if(typeFactors == null) {
            initTypeFactors();
        }
        return typeFactors.get(attackerType).get(defenderType);
    }

    private static void initTypeFactors() {
        typeFactors = new EnumMap<>(Type.class);
        InputStream typeStream = MoveFactory.class.getResourceAsStream("/typeFactors");
        Scanner scanner = new Scanner(typeStream).useLocale(Locale.GERMAN);     // use custom local so every machine can run the server
        for (Type attackType:
             Type.values()) {
            EnumMap<Type, Double> attackTypeValues = new EnumMap<>(Type.class);
            for (Type defenderType:
                 Type.values()) {
                double val = scanner.nextDouble();
                attackTypeValues.put(defenderType, val);
            }
            typeFactors.put(attackType, attackTypeValues);
        }
    }

}
