package networking;

import trainer.Action;
import trainer.ServerPlayer;
import trainer.Trainer;

public interface ServerI {
    void gotRoomSelection(ServerPlayer player, int room_num);
}
