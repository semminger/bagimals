package networking;

import bagimal.Bagimal;
import controller.GameController;
import trainer.Action;
import trainer.ServerPlayer;
import trainer.Trainer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Room extends GameController {

    private Action firstAction;
    private List<ServerPlayer> activePlayer = new ArrayList<>();

    private boolean inForceSwap = false;

    @Override
    protected void requestAction() {
        firstAction = null;
        ServerPlayer player1 = activePlayer.get(0);
        ServerPlayer player2 = activePlayer.get(1);

        if(player1.getCurrentBagimal().hasStatus(Bagimal.Status.CHARGING) ||
                player1.getCurrentBagimal().hasStatus(Bagimal.Status.RECHARGING)) {
            retrieveAction(new Action(player1.getCurrentBagimal().getChargeMoveNumber(), player1, player2));
        } else {
            player1.action(player2);
        }
        if(player2.getCurrentBagimal().hasStatus(Bagimal.Status.CHARGING) ||
                player2.getCurrentBagimal().hasStatus(Bagimal.Status.RECHARGING)) {
            retrieveAction(new Action(player2.getCurrentBagimal().getChargeMoveNumber(), player2, player1));
        } else {
            player2.action(player1);
        }
    }

    @Override
    protected void requestForceSwap(Trainer trainer) {
        if(fightNotOver()) {
            inForceSwap = true;
            trainer.forceSwap();
        } else {
            updatePlayers();
            endFight();
        }
    }

    @Override
    protected void endFight() {
        ServerPlayer player1 = (ServerPlayer) trainers.get(0);
        ServerPlayer player2 = (ServerPlayer) trainers.get(1);
        if(player1.isFightable()) {
            broadcastMessage(player1.getName() + " has WON!");
            player1.sendEndResult("WON");
            player2.sendEndResult("LOST");
        } else {

            broadcastMessage(player2.getName() + " has WON!");
            player1.sendEndResult("LOST");
            player2.sendEndResult("WON");
        }
        // Clear Room
        closeActivePlayersConnection();
    }

    @Override
    public void startFight(Trainer trainer1, Trainer trainer2) {
        super.startFight(trainer1, trainer2);

        ((ServerPlayer) trainer1).sendOpponentName(trainer2.getName());
        ((ServerPlayer) trainer2).sendOpponentName(trainer1.getName());

        updatePlayers();
        requestAction();
    }

    public void addPlayer(ServerPlayer newPlayer) {
        if(activePlayer.size() < 2) {
            newPlayer.setRoom(this);
            activePlayer.add(newPlayer);

            if(activePlayer.size() == 2) {
                startFight(activePlayer.get(0), newPlayer);
            }
        }
    }

    private void updatePlayers() {
        ServerPlayer player1 = activePlayer.get(0);
        ServerPlayer player2 = activePlayer.get(1);

        player1.updatePlayer(player2);
        player2.updatePlayer(player1);
    }

    public void gotForceSwapUpdate(Trainer trainer) {
        broadcastMessage(trainer.getName() + " puts " + trainer.getCurrentBagimal().getName() + " into the fight");
        inForceSwap = false;
        updatePlayers();
        requestAction();
    }

    public void broadcastMessage(String message) {
        ((ServerPlayer)trainers.get(0)).writeToPlayer(message);
        ((ServerPlayer)trainers.get(1)).writeToPlayer(message);
        System.out.println(message);
    }

    public void retrieveAction(Action action) {
        if(firstAction == null) {
            firstAction = action;
        } else {
            broadcastMessage(executeRound(determineQue(firstAction, action)));
            updatePlayers();
            // Dont ask for Action when one player needs to force swap
            if(!inForceSwap && fightNotOver()) {
                requestAction();
            }
        }
    }

    public void playerDisconnected(ServerPlayer player) {
        ServerPlayer otherPlayer;
        if(activePlayer.size() == 2) {
            if (player == activePlayer.get(0)) {
                otherPlayer = activePlayer.get(1);
            } else {
                otherPlayer = activePlayer.get(0);
            }
            otherPlayer.writeToPlayer(player.getName() + " disconnected!");
            closeActivePlayersConnection();
        } else {
            try {
                player.closeConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            activePlayer.clear();
            System.out.println(player.getName() + " disconnected");
        }
    }

    public void closeActivePlayersConnection() {
        try {
            for (ServerPlayer player:
                    activePlayer) {
                player.closeConnection();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Clear Player list
        activePlayer.clear();
    }

    public boolean roomIsFull() {
        return activePlayer.size() == 2;
    }

    public int getRoomOccupation() {
        return activePlayer.size();
    }

    public void closeRoom() {
        closeActivePlayersConnection();
    }
}
