package networking;

import trainer.Trainer;

import javax.swing.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.Buffer;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

public class ClientStartInput {
    private JTextField ipField;
    private JPanel panel;
    private JTextField nameField;
    private JList<String> allBagimalList;
    private JButton connectButton;
    private JList<String> chosenBagimalList;
    private JButton removeButton;

    private DefaultListModel<String> chosenBagimalModel = new DefaultListModel<>();
    ArrayList<String> bagimalListString;

    public ClientStartInput(Client client) {

        chosenBagimalList.setModel(chosenBagimalModel);
        allBagimalList.setTransferHandler(new ExportTransferHandler());
        chosenBagimalList.setTransferHandler(new ImportTransferHandler());

        removeButton.addActionListener(actionEvent -> {
            int index = chosenBagimalList.getSelectedIndex();

            if(index != -1) {
                chosenBagimalModel.remove(chosenBagimalList.getSelectedIndex());
                chosenBagimalList.validate();
            }
        });

        connectButton.addActionListener(actionEvent -> {

            String ip = ipField.getText().replace(";", "");
            String name = nameField.getText().replace(";", "");
            String bagimalsString = chosenBagimalModel.toString().substring(1, chosenBagimalModel.toString().length()-1);   // This is stupid but what ever
            if(chosenBagimalModel.size() == Trainer.MAX_BAGIMALS && bagimalListString.containsAll(Arrays.asList(chosenBagimalModel.toArray())) && !name.isEmpty()) {
                client.connectToServer(ip, name, bagimalsString);
            }
        });


    }

    private class ExportTransferHandler extends TransferHandler {
        public int getSourceActions(JComponent c){
            return TransferHandler.COPY;
        }

        public Transferable createTransferable(JComponent c) {
            return new StringSelection(allBagimalList.getSelectedValue());
        }
    }

    private class ImportTransferHandler extends TransferHandler {

        public boolean canImport(TransferHandler.TransferSupport supp) {
            return supp.isDataFlavorSupported(DataFlavor.stringFlavor);
        }

        public Transferable createTransferable(JComponent c) {
            return new StringSelection(chosenBagimalList.getSelectedValue());
        }

        public int getSourceActions(JComponent c){
            return TransferHandler.MOVE;
        }

        public boolean importData(TransferHandler.TransferSupport supp) {
            // Fetch the Transferable and its data
            Transferable t = supp.getTransferable();
            String data;
            try {
                data = (String)t.getTransferData(DataFlavor.stringFlavor);
            } catch (Exception e){
                System.out.println(e.getMessage());
                return false;
            }

            // Fetch the drop location
            JList.DropLocation loc = chosenBagimalList.getDropLocation();
            int row = loc.getIndex();


            // Used to move Bagimals around
            if((MOVE & supp.getSourceDropActions()) == MOVE) {
                chosenBagimalModel.add(row, data);
                chosenBagimalModel.remove(chosenBagimalList.getSelectedIndex());
            } else if((COPY & supp.getSourceDropActions()) == COPY && chosenBagimalModel.size() < Trainer.MAX_BAGIMALS) {
                chosenBagimalModel.add(row, data);
            }
            chosenBagimalList.validate();
            return true;
        }
    }

    public JPanel getPanel() {
        return panel;
    }

    private void createUIComponents() {
        //File bagimalDir = new File(Paths.get("", "bagimals").toAbsolutePath().toString());
        bagimalListString = new ArrayList<>();
        //bagimalDir.listFiles();
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/allBagimals.txt")));
            String name;
            while((name = in.readLine()) != null) {
                bagimalListString.add(name);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        allBagimalList = new JList<>(bagimalListString.toArray(new String[0]));
    }
}
