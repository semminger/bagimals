package networking;



import bagimal.Bagimal;
import bagimal.BagimalFactory;
import bagimal.Stat;
import controller.GameController;


import javax.swing.*;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import trainer.ServerPlayer;



public class Server implements ServerI{

    private ServerSocket serverSocket;
    private final int port = 7777;
    private JTextArea outArea;
    private JPanel panel;
    private final int LEVEL_CAP = 50;

    private final String VERSION = "V1.1";
    private final int MAX_ROOM_NUM = 5;

    private Room[] rooms = new Room[MAX_ROOM_NUM];

    private Server() {

        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < MAX_ROOM_NUM; i++) {
            rooms[i] = new Room();
        }
    }



    public void acceptNewPlayers() {


        while(true) {
            try {
                connectPlayer();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void closeServer() {
        try {
            serverSocket.close();
            for (int i = 0; i < MAX_ROOM_NUM; i++) {
                rooms[i].closeRoom();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }

    private void connectPlayer() throws IOException {
        Socket playerSocket =  serverSocket.accept();

        PrintWriter out;
        BufferedReader in;
        try {
            out = new PrintWriter(playerSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(playerSocket.getInputStream()));

            //CHECK VERSION
            String clientVersion = in.readLine().split(";")[1];
            if(clientVersion.equals(VERSION))  {
                out.println("OK;");
                // Need to split to remove Command... maybe check for command but i donno
                String name = in.readLine().split(";")[1];
                String[] bagimalsString = in.readLine().split(";")[1].split(",");
                out.println("LEVEL_CAP;" + LEVEL_CAP);
                out.println("ROOMS;" + getRoomOccupations());

                List<Bagimal> bagimals = new ArrayList<>();
                for (String bagimal:
                        bagimalsString) {
                    try {
                        Bagimal newBagimal = BagimalFactory.createBagimal(bagimal.trim(), null, null, LEVEL_CAP);
                        bagimals.add(newBagimal);
                        String iv_msg = "IVS;" + newBagimal.getHpIV() + ",";
                        for (Stat stat:
                                Stat.values()) {
                            iv_msg += newBagimal.getStatIV(stat) + ",";
                        }
                        out.println(iv_msg);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                new ServerPlayer(name, bagimals, playerSocket, this);
                outArea.append(name + " has connected!\n");
            } else {
                out.println("WRONG_VERSION;");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void gotRoomSelection(ServerPlayer player, int room_num) {
        if(!rooms[room_num].roomIsFull()) {
            player.writeToPlayer("OK;");
            rooms[room_num].addPlayer(player);
        } else {
            player.writeToPlayer("FULL;");
        }
    }

    private String getRoomOccupations() {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < MAX_ROOM_NUM; i++) {
            output.append(rooms[i].getRoomOccupation()).append(",");
        }
        return output.toString();
    }

    public static void main(String[] args) {
        Server server = new Server();
        JFrame frame = new JFrame("Server");
        frame.setContentPane(server.panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setSize(800,600);
        server.acceptNewPlayers();
    }

}
