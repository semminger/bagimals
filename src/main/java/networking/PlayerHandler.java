package networking;


import output.OutputController;
import trainer.ServerPlayer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class PlayerHandler extends Thread {
    Socket playerSocket;
    PrintWriter out;
    BufferedReader in;
    ServerPlayer player;

    public PlayerHandler(Socket playerSocket, ServerPlayer player) {
        this.playerSocket = playerSocket;
        this.player = player;
        try {
            out = new PrintWriter(playerSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(playerSocket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.start();
    }
    @Override
    public void run() {
        String inputLine;
        try {
            while ((inputLine = in.readLine()) != null) {
                player.newPlayerUpdate(inputLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeToPlayer(String msg) {
        out.println(msg);
    }

    public void closeConnection() throws IOException {
        playerSocket.close();
    }
}
