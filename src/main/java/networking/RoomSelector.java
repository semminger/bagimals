package networking;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RoomSelector {
    private final int MAX_ROOM_NUM = 5;

    private JPanel panel;
    private JButton room1Button;
    private JButton room2Button;
    private JButton room3Button;
    private JButton room4Button;
    private JButton room5Button;
    private JLabel roomSelectorLabel;

    Client client;

    private final JButton[] roomButtons = {room1Button, room2Button, room3Button, room4Button, room5Button};

    public RoomSelector(Client client) {
        this.client = client;
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setRoomInformation(String[] roomOccupations) {
        for (int i = 0; i < MAX_ROOM_NUM; i++) {
            roomButtons[i].addActionListener(new SelectRoomListener(i, Integer.parseInt(roomOccupations[i])));
            roomButtons[i].setText("Room " + (i + 1) + " : " + roomOccupations[i] + "/2");
        }

        roomSelectorLabel.setText("Please select a room!");
    }

    private class SelectRoomListener implements ActionListener {

        int room_num, roomOccupation;

        public SelectRoomListener(int room_num, int roomOccupation) {
            this.room_num = room_num;
            this.roomOccupation = roomOccupation;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if(roomOccupation < 2) {
                roomSelectorLabel.setText(client.selectRoom(room_num));
            } else {
                roomSelectorLabel.setText("This room is full!");
            }
        }
    }
}
