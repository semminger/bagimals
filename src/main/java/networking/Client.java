package networking;

import bagimal.Bagimal;
import bagimal.BagimalFactory;
import bagimal.Stat;
import trainer.Player;

import javax.swing.*;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.net.ConnectException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

public class Client extends JFrame{
    //private String hostname = "localhost";
    private final int port = 7777;
    private final String VERSION = "V1.1";

    private PrintWriter out;
    private BufferedReader in;
    private Socket sSocket = null;

    private JTextArea outArea;
    private JPanel panel;
    private JPanel inputPanel;
    private JButton fightButton;
    private JButton teamButton;
    private JPanel startPanel;
    private JPanel teamPanel;
    private JButton bagimal1Button;
    private JButton bagimal2Button;
    private JButton bagimal3Button;
    private JButton bagimal4Button;
    private JProgressBar enemyHealthBar;
    private JProgressBar selfHealthBar;
    private JLabel enemyBagimalLabel;
    private JLabel playerBagimalLabel;
    private JPanel waitPanel;
    private JPanel movePanel;
    private JButton move1Button;
    private JButton move2Button;
    private JButton move3Button;
    private JButton move4Button;
    private JLabel waitLabel;
    private JButton backMoveButton;
    private JButton backTeamButton;
    private JLabel enemyStatus;
    private JLabel playerStatus;
    private JPanel imagePanel;
    private JLabel playerImageLabel;
    private JLabel enemyImageLabel;
    private JPanel playerImagePanel;
    private JTextField inField;
    private JButton sendButton;

    private JPanel masterPanel;
    private RoomSelector rmSelector;

    Player player;
    String enemyName;
    JButton[] moveButtons = new JButton[]{move1Button, move2Button, move3Button, move4Button};
    JButton[] bagimalButtons = {bagimal1Button, bagimal2Button, bagimal3Button, bagimal4Button};
    boolean isForceSwap = false;

    public Client() {
        super("Client");

        // Card Layout to switch from the team picker to the battle
        masterPanel = new JPanel(new CardLayout());
        masterPanel.add(new ClientStartInput(this).getPanel());
        rmSelector = new RoomSelector(this);
        masterPanel.add(rmSelector.getPanel());
        masterPanel.add(panel);

        setContentPane(masterPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
        setSize(800,600);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if(sSocket != null) {
                    out.println("DISCONNECT;");
                    try {
                        sSocket.close();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            }
        });

        // Show the waiting for opponent screen
        ((CardLayout) inputPanel.getLayout()).last(inputPanel);

        // Activate auto scroll
        ((DefaultCaret) outArea.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

        move1Button.addActionListener(actionEvent -> chooseMove(0));
        move2Button.addActionListener(actionEvent -> chooseMove(1));
        move3Button.addActionListener(actionEvent -> chooseMove(2));
        move4Button.addActionListener(actionEvent -> chooseMove(3));

        bagimal1Button.addActionListener(actionEvent -> chooseSwapBagimal(0));
        bagimal1Button.addMouseListener(new BagimalMouseListener(bagimal1Button, 0));
        bagimal2Button.addActionListener(actionEvent -> chooseSwapBagimal(1));
        bagimal2Button.addMouseListener(new BagimalMouseListener(bagimal2Button, 1));
        bagimal3Button.addActionListener(actionEvent -> chooseSwapBagimal(2));
        bagimal3Button.addMouseListener(new BagimalMouseListener(bagimal3Button, 2));
        bagimal4Button.addActionListener(actionEvent -> chooseSwapBagimal(3));
        bagimal4Button.addMouseListener(new BagimalMouseListener(bagimal4Button, 3));

        fightButton.addActionListener(actionEvent -> ((CardLayout)inputPanel.getLayout()).show(inputPanel, "moves"));

        teamButton.addActionListener(actionEvent -> ((CardLayout)inputPanel.getLayout()).show(inputPanel, "team"));

        backMoveButton.addActionListener(actionEvent -> ((CardLayout)inputPanel.getLayout()).show(inputPanel, "start"));

        backTeamButton.addActionListener(actionEvent -> {
            if(!isForceSwap) {
                ((CardLayout) inputPanel.getLayout()).show(inputPanel, "start");
            }
        });

    }

    private void updateSelfBagimalImage() {
        String bagimalName = player.getCurrentBagimal().getName();
        URL imageURL = getClass().getResource("/sprites/" + bagimalName + "_back.png");
        ImageIcon icon = new ImageIcon(imageURL);
        if(player.getCurrentBagimal().isAlive())
            playerImageLabel.setIcon(icon);
        else
            playerImageLabel.setIcon(null);
        playerImageLabel.setSize(icon.getImage().getWidth(null), icon.getImage().getHeight(null));
    }

    private void updateSelfBagimal() {
        Bagimal curBagimal = player.getCurrentBagimal();
        playerBagimalLabel.setText("YOU: " + curBagimal.getName());
        int bagimalHpPercentage = (int) Math.ceil((curBagimal.getHp()*100)/(double)curBagimal.getMax_hp());
        selfHealthBar.setValue(bagimalHpPercentage);
        Bagimal.Status status = curBagimal.getCurrentNonVolatileStatus();
        if(status != null)
            playerStatus.setText(status.toString());
        else
            playerStatus.setText("");
    }

    private void updateEnemyBagimalImage(String name, boolean alive) {
        URL imageURL = getClass().getResource("/sprites/" + name + "_front.png");
        ImageIcon icon = new ImageIcon(imageURL);
        if(alive)
            enemyImageLabel.setIcon(icon);
        else
            enemyImageLabel.setIcon(null);
        enemyImageLabel.setSize(icon.getImage().getWidth(null), icon.getImage().getHeight(null));
    }

    private void updateEnemyBagimal(String name, int healthPercentage, String status) {
        enemyBagimalLabel.setText(enemyName + ": " + name);
        enemyHealthBar.setValue(healthPercentage);
        if(!status.equals("NONE"))
            enemyStatus.setText(status);
        else
            enemyStatus.setText("");

        updateEnemyBagimalImage(name, healthPercentage != 0);
    }

    private void updateMoves() {
        Bagimal curBagimal = player.getCurrentBagimal();

        for(int i = 0; i < curBagimal.getMoveCount(); i++) {
            moveButtons[i].setText(curBagimal.getMove(i).getName() + " : " + curBagimal.getMovePP(i));
            if(curBagimal.getMovePP(i) == 0) {
                moveButtons[i].setBackground(Color.RED);
            }
        }
    }


    private void updateBagimals() {
        for(int i = 0; i < player.getTeamSize(); i++) {
            bagimalButtons[i].setText(player.getBagimal(i).getName());
        }
    }

    private void updateTeamLiveStatus() {
        for(int i = 0; i < player.getTeamSize(); i++) {
            if(!player.getBagimal(i).isAlive()) {
                bagimalButtons[i].setBackground(Color.RED);
            }
        }
    }

    private void showEndScreen(String endResult) {
        waitLabel.setText("YOU " + endResult);
        ((CardLayout) inputPanel.getLayout()).last(inputPanel);
    }

    private void chooseMove(int move_num) {
        Bagimal currBagimal = player.getCurrentBagimal();
        if(currBagimal.getMoveCount() > move_num && currBagimal.getMovePP(move_num) > 0) {
            currBagimal.reduceMovePP(move_num);
            updateMoves();
            out.println("ATTACK;" + move_num);
            ((CardLayout) inputPanel.getLayout()).last(inputPanel);
        }
    }

    private void chooseSwapBagimal(int bag_num) {
        if(player.getTeamSize() > bag_num && player.getBagimal(bag_num) != player.getCurrentBagimal()) {
            // Update the player
            player.swapBagimal(bag_num);
            updateSelfBagimal();
            updateSelfBagimalImage();
            updateMoves();

            // Update the Server
            if(isForceSwap) {
                out.println("FORCE_SWAP;" + bag_num);
                isForceSwap = false;
                ((CardLayout)inputPanel.getLayout()).show(inputPanel, "start");
            } else {
                out.println("SWAP;" + bag_num);
            }
            // Dont really need to switch?
            ((CardLayout) inputPanel.getLayout()).last(inputPanel);
        }
    }

    private void parseServerMessage(String message) {
        if(message.contains(";")) {
            String[] params = message.split(";");
            switch(params[0]) {
                case "UPDATE_SELF":
                    player.getCurrentBagimal().reduceHp(player.getCurrentBagimal().getHp() - Integer.parseInt(params[1]));  // Pretty weird to take the health end reduce it by the new health...
                    if(!params[2].equals("NONE")) {
                        player.getCurrentBagimal().giveStatus(Bagimal.Status.valueOf(params[2]));
                    }
                    updateSelfBagimal();
                    updateSelfBagimalImage();
                    updateTeamLiveStatus();
                    break;

                case "UPDATE_ENEMY":
                    updateEnemyBagimal(params[1], Integer.parseInt(params[2]), params[3]);
                    break;

                case "REQUEST_FORCE_SWAP":
                    outArea.append("Please choose a new Bagimal\n");
                    isForceSwap = true;
                    ((CardLayout)inputPanel.getLayout()).show(inputPanel, "team");
                    break;

                case "REQUEST_ACTION":
                    ((CardLayout)inputPanel.getLayout()).show(inputPanel, "start");
                    break;

                case "GAME_OVER":
                    showEndScreen(params[1]);
                    break;

                case "ENEMY_NAME":
                    enemyName = params[1];
                    break;

            }
            System.out.println(message);
        }
        else {
            outArea.append(message + "\n");
        }
    }

    private boolean checkVersion() throws IOException {
        out.println("VERSION;" + VERSION);
        return in.readLine().equals("OK;");
    }

    public void connectToServer(String hostname, String name, String bagimalsString) {
        try {
            ((CardLayout)masterPanel.getLayout()).next(masterPanel);    // Switch to room select

            sSocket = new Socket(hostname, port);
            out = new PrintWriter(sSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(sSocket.getInputStream()));

            if(checkVersion()) {
                out.println("NAME;" + name);
                out.println("BAGIMALS;" + bagimalsString);
                // Need to split to remove Command... maybe check for command but i donno
                int level_cap = Integer.parseInt(in.readLine().split(";")[1]);
                rmSelector.setRoomInformation(in.readLine().split(";")[1].split(","));

                List<Bagimal> bagimals = new ArrayList<>();
                for (String bagimal :
                        bagimalsString.split(",")) {
                    try {
                        String[] ivsString = in.readLine().split(";")[1].split(",");
                        int hp_iv = Integer.parseInt(ivsString[0]);
                        EnumMap<Stat, Integer> ivs = new EnumMap<>(Stat.class);
                        int i = 1;
                        for (Stat stat:
                             Stat.values()) {
                            ivs.put(stat, Integer.parseInt(ivsString[i]));
                            i++;
                        }
                        bagimals.add(BagimalFactory.createBagimal(bagimal.trim(), hp_iv, ivs, level_cap));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                player = new Player(name, bagimals);

            } else {
                outArea.setText("YOUR VERSION DOESN'T MATCH WITH THE SERVER");
            }

        } catch (UnknownHostException | ConnectException e) {
            outArea.setText("COULDN'T CONNECT TO SERVER!");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void initBattle() {
        updateMoves();
        updateBagimals();
        updateSelfBagimalImage();

        new Thread(() -> {
            try {
                String line;
                while ((line = in.readLine()) != null) {
                    parseServerMessage(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public String selectRoom(int room_num) {
        out.println("ROOM;" + room_num);
        try {
            String serverAnswer = in.readLine();
            switch(serverAnswer) {
                case "OK;":
                    ((CardLayout)masterPanel.getLayout()).next(masterPanel);
                    initBattle();
                    break;

                case "FULL;":
                    return "Room " + (room_num + 1) + " is full";

                default:
                    return "Something went wrong";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void main(String[] args) {
       new Client();
    }

    public class BagimalMouseListener extends MouseAdapter {

        private final int bag_num;
        private final JButton bagimalButton;

        public BagimalMouseListener(JButton bagimalButton, int bag_num) {
            this.bag_num = bag_num;
            this.bagimalButton = bagimalButton;
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            if(player.getTeamSize() > bag_num) {
                Bagimal bagimal = player.getBagimal(bag_num);
                Bagimal.Status nonVolStatus = bagimal.getCurrentNonVolatileStatus();
                String statusString = (nonVolStatus == null) ? "" : " : " + nonVolStatus;
                bagimalButton.setText(bagimal.getHp() + "/" + bagimal.getMax_hp() + statusString);
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
            if(player.getTeamSize() > bag_num) {
                bagimalButton.setText(player.getBagimal(bag_num).getName());
            }
        }
    }

}
