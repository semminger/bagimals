package creators;

import type.Type;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public class BagimalCreator {
    private JTextField nameField;
    private JPanel panel;
    private JSlider hpSlider;
    private JSlider atkSlider;
    private JSlider defSlider;
    private JSlider spdSlider;
    private JTextField descField;
    private JList<String> moveList;
    private JComboBox<Type> typeComboBox;
    private JButton createButton;
    private JLabel defValue;
    private JLabel atkValue;
    private JLabel hpValue;
    private JLabel spdValue;
    private JTextArea outputLabel;
    private JScrollPane scrollPane;
    private JSlider spAtkSlider;
    private JSlider spDefSlider;
    private JLabel spDefValue;
    private JLabel spAtkValue;

    public BagimalCreator() {

        hpSlider.addChangeListener(changeEvent -> hpValue.setText("HP: " + hpSlider.getValue()));

        atkSlider.addChangeListener(changeEvent -> atkValue.setText("ATK: " + atkSlider.getValue()));

        defSlider.addChangeListener(changeEvent -> defValue.setText("DEF: " + defSlider.getValue()));

        spAtkSlider.addChangeListener(changeEvent -> spAtkValue.setText("SP_ATK: " + spAtkSlider.getValue()));

        spDefSlider.addChangeListener(changeEvent -> spDefValue.setText("SP_DEF: " + spDefSlider.getValue()));

        spdSlider.addChangeListener(changeEvent -> spdValue.setText("SPD: " + spdSlider.getValue()));

        createButton.addActionListener(actionEvent -> {
            String name = nameField.getText();
            String desc = descField.getText();
            String hp = String.valueOf(hpSlider.getValue());
            String atk = String.valueOf(atkSlider.getValue());
            String def = String.valueOf(defSlider.getValue());
            String sp_atk = String.valueOf(spAtkSlider.getValue());
            String sp_def = String.valueOf(spDefSlider.getValue());
            String spd = String.valueOf(spdSlider.getValue());
            Type type = (Type)typeComboBox.getSelectedItem();
            List<String> moveSelections = moveList.getSelectedValuesList();

            if(name.isEmpty()) {
                outputLabel.setText("Please enter a name!");
            } else if(desc.isEmpty()) {
                outputLabel.setText("Please enter a description!");
            } else if(moveSelections.size() > 4) {
                outputLabel.setText("Please only choose FOUR moves!");
            } else {

                String fileString =
                        "[name]\n" + name
                                + "\n\n[base_hp]\n" + hp
                                + "\n\n[base_atk]\n" + atk
                                + "\n\n[base_def]\n" + def
                                + "\n\n[base_sp_atk]\n" + sp_atk
                                + "\n\n[base_sp_def]\n" + sp_def
                                + "\n\n[base_spd]\n" + spd
                                + "\n\n[type]\n" + type
                                + "\n\n[desc]\n" + desc
                                + "\n\n[moves]\n" + moveSelections.toString().substring(1, moveSelections.toString().length()-1);   // This is stupid but what ever

                System.out.println(fileString);
                try {
                    Path outPath = Paths.get("bagimals",name.replace(" ", "_") + ".bag");
                    String path = outPath.toAbsolutePath().toString();
                    BufferedWriter writer = new BufferedWriter(new FileWriter(path));
                    writer.write(fileString);
                    writer.close();
                    outputLabel.setText("Yeah you created a Bagimal!\nSaved to: " + path);
                } catch (IOException e) {
                    outputLabel.setText("There was a problem: " + e.toString());
                }
            }
        });

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Bagimal Creator");
        frame.setContentPane(new BagimalCreator().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setSize(800,600);
    }

    private void createUIComponents() {

        File moveDir = new File(Paths.get("", "moves").toAbsolutePath().toString());
        ArrayList<String> moveListString = new ArrayList<>();
        for (File file:
                moveDir.listFiles()) {
            moveListString.add(file.getName().replace(".mv",""));
        }

        System.out.println(moveListString);
        moveList = new JList<>(moveListString.toArray(new String[0]));
        typeComboBox = new JComboBox<>(Type.values());
        System.out.println(typeComboBox.getItemAt(0));

    }
}
