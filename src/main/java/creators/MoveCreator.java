package creators;

import bagimal.Bagimal.Status;
import bagimal.StageStat;
import bagimal.Stat;
import moves.DamageCategory;
import moves.MultipleHitType;
import type.Type;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MoveCreator {
    private JPanel panel;
    private JTextField nameField;
    private JSlider powSlider;
    private JSlider accSlider;
    private JTextField descField;
    private JTabbedPane inflictionTab;
    private JComboBox<Type> typeComboBox;
    private JComboBox<Status> statusComboBox;
    private JSlider statusProbSlider;
    private JCheckBox statusOnSelfCheckBox;
    private JComboBox<StageStat> statComboBox;
    private JSlider statAmountSlider;
    private JSlider statProbSlider;
    private JCheckBox statOnSelfCheck;
    private JSlider healRecoilSlider;
    private JButton createButton;
    private JLabel statusProbLabel;
    private JLabel statAmountLabel;
    private JLabel statProbLabel;
    private JLabel healRecoilPercLabel;
    private JLabel powLabel;
    private JLabel accLabel;
    private JTextArea outputLabel;
    private JTabbedPane behaviourTab;
    private JComboBox<MultipleHitType> hitsComboBox;
    private JTextField chargingLineField;
    private JComboBox<DamageCategory> dmgCatComboBox;
    private JCheckBox cannotMissCheckBox;
    private JLabel ppLabel;
    private JSlider ppSlider;
    private JCheckBox isRechargingCheckBox;

    public MoveCreator() {
        powSlider.addChangeListener(changeEvent -> powLabel.setText("POW: " + powSlider.getValue()));

        accSlider.addChangeListener(changeEvent -> accLabel.setText("ACC: " + accSlider.getValue()));

        ppSlider.addChangeListener(changeEvent -> ppLabel.setText("PP: " + ((ppSlider.getValue() == 0) ? 1 : ppSlider.getValue() * 5)));

        statusProbSlider.addChangeListener(changeEvent -> statusProbLabel.setText("Probability: " + statusProbSlider.getValue() + "%"));

        statAmountSlider.addChangeListener(changeEvent -> statAmountLabel.setText("Amount: " + statAmountSlider.getValue()));

        statProbSlider.addChangeListener(changeEvent -> statProbLabel.setText("Probability: " + statProbSlider.getValue() + "%"));

        healRecoilSlider.addChangeListener(changeEvent -> healRecoilPercLabel.setText("Heal/Recoil percentage: " + healRecoilSlider.getValue() + "%"));

        createButton.addActionListener(actionEvent -> {
            String name = nameField.getText();
            String desc = descField.getText();
            DamageCategory dmg_cat = (DamageCategory) dmgCatComboBox.getSelectedItem();
            int pow = (dmg_cat != DamageCategory.STATUS) ? powSlider.getValue() : 0;
            int acc = accSlider.getValue();
            int pp = (ppSlider.getValue() == 0) ? 1 : ppSlider.getValue() * 5;
            Type type = (Type)typeComboBox.getSelectedItem();

            boolean cannotmiss = cannotMissCheckBox.isSelected();
            String infliction = "";
            int inflictionTyp = inflictionTab.getSelectedIndex();
            int moveBehaviourTyp = behaviourTab.getSelectedIndex();

            switch (inflictionTyp) {
                case 1:
                    infliction = "\n\n[infliction]\nSTATUS," + statusComboBox.getSelectedItem()+ "," + statusProbSlider.getValue() + "," + statusOnSelfCheckBox.isSelected();
                    break;

                case 2:
                    infliction = "\n\n[infliction]\nSTAT_BUFF," + statComboBox.getSelectedItem() + "," + statAmountSlider.getValue() + "," +statProbSlider.getValue() + "," + statOnSelfCheck.isSelected();
                    break;

                case 3:
                    double healRecoilPerc = healRecoilSlider.getValue()/100.0;
                    infliction = "\n\n[infliction]\nHEALING_RECOIL," + healRecoilPerc;
                    break;
            }

            String fileString = "";

            switch (moveBehaviourTyp) {
                case 0:
                    fileString = "[move_beh]\nGENERAL";
                    break;

                case 1:
                    fileString = "[move_beh]\nMULTI_HIT" +
                            "\n\n[hits]\n" + hitsComboBox.getSelectedItem();
                    break;

                case 2:
                    fileString = "[move_beh]\nCHARGE" +
                            "\n\n[charge_line]\n" + chargingLineField.getText() +
                            "\n\n[isRecharging]\n" + isRechargingCheckBox.isSelected();

            }

            fileString +=
                    "\n\n[name]\n" + name
                            + "\n\n[desc]\n" + desc
                            + "\n\n[pow]\n" + pow
                            + "\n\n[acc]\n" + acc
                            + "\n\n[pp]\n" + pp
                            + "\n\n[type]\n" + type
                            + "\n\n[dmg_cat]\n" + dmg_cat
                            + "\n\n[cannotmiss]\n" + cannotmiss
                            + infliction;

            System.out.println(fileString);
            try {
                Path outPath = Paths.get("moves",name.replace(" ", "_") + ".mv");
                String path = outPath.toAbsolutePath().toString();
                BufferedWriter writer = new BufferedWriter(new FileWriter(path));
                writer.write(fileString);
                writer.close();
                outputLabel.setText("Yeah you created a Bagimal move!\nSaved to: " + path);
            } catch (IOException e) {
                outputLabel.setText("There was a problem: " + e.toString());
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Move Creator");
        frame.setContentPane(new MoveCreator().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setSize(800,600);
    }

    private void createUIComponents() {
        typeComboBox = new JComboBox<>(Type.values());
        statComboBox = new JComboBox<>(StageStat.values());
        statusComboBox = new JComboBox<>(Status.values());
        statusComboBox.removeItem(Status.CHARGING);
        statusComboBox.removeItem(Status.RECHARGING);
        hitsComboBox = new JComboBox<>(MultipleHitType.values());
        dmgCatComboBox = new JComboBox<>(DamageCategory.values());
    }
}
