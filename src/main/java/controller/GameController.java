package controller;

import bagimal.Stat;
import output.OutputController;
import trainer.Action;
import trainer.Trainer;

import java.util.ArrayList;
import java.util.List;


public abstract class GameController {
    protected List<Trainer> trainers = new ArrayList<>();

    public GameController() {

    }

    protected String executeRound(List<Action> actionQue) {
        // Do the que
       // boolean skipTurn = false;
        String output = "";

        output += "--------------------\n";

        for (Action action:
                actionQue) {

            // To hard-skip out of the turn
            //if(skipTurn)
             //   break;

            output += action.getTrainer().getName() + ":\n";

            switch(action.getAction()) {

                case ATTACK:
                    output += action.getTrainer().attack(action.getActionNumber(), action.getOtherTrainer());
                    //action.getTrainer().getCurrentBagimal().attack(action.getActionNumber(), action.getOtherTrainer().getCurrentBagimal());

                    // Other Bagimal got killed lel
                    if(!action.getOtherTrainer().getCurrentBagimal().isAlive()) {


                        requestForceSwap(action.getOtherTrainer());
                        return output;
                        // Turn is over, skip the rest
                        //skipTurn = true;
                    }
                    break;

                case SWAP:
                    action.getTrainer().swapBagimal(action.getActionNumber());
                    output += action.getTrainer().getName() + " puts " + action.getTrainer().getCurrentBagimal().getName() + " into the fight\n";
                    break;

                case ITEM:
                    action.getTrainer().useItem(action.getItemTargetNumber(), action.getActionNumber());
                    break;

                default:
                    OutputController.write("This should not happen!");
                    break;
            }
            output += "\n\n";
        }

        // Do status effects
        for (Action action:
                actionQue) {
            Trainer trainer = action.getTrainer();
            output += trainer.getCurrentBagimal().executeStatusEffects();

            if(!trainer.getCurrentBagimal().isAlive()) {
                requestForceSwap(trainer);
                return output;
            }
        }

        output += "--------------------\n";
        return output;
    }

    protected List<Action> determineQue(Action player1Action, Action player2Action) {
        List<Action> actionQue = new ArrayList<>();
        // ITEM
        if(player1Action.getAction() == Action.Action_Event.ITEM) {
            actionQue.add(player1Action);
        }
        if(player2Action.getAction() == Action.Action_Event.ITEM) {
            actionQue.add(player2Action);
        }

        // SWAP
        if(player1Action.getAction() == Action.Action_Event.SWAP) {
            actionQue.add(player1Action);
        }
        if(player2Action.getAction() == Action.Action_Event.SWAP) {
            actionQue.add(player2Action);
        }

        // ATTACK
        if(player1Action.getAction() == Action.Action_Event.ATTACK && player2Action.getAction() == Action.Action_Event.ATTACK) {
            if(player1Action.getTrainer().getCurrentBagimal().getStatValue(Stat.SPD) >= player2Action.getTrainer().getCurrentBagimal().getStatValue(Stat.SPD)) {
                actionQue.add(player1Action);
                actionQue.add(player2Action);
            } else {
                actionQue.add(player2Action);
                actionQue.add(player1Action);
            }
        } else{
            if(player1Action.getAction() == Action.Action_Event.ATTACK) {
                actionQue.add(player1Action);
            }
            if(player2Action.getAction() == Action.Action_Event.ATTACK) {
                actionQue.add(player2Action);
            }
        }

        return actionQue;
    }

    protected abstract void requestAction();

    protected abstract void requestForceSwap(Trainer trainer);

    public void startFight(Trainer trainer1, Trainer trainer2) {
        trainers = new ArrayList<>();
        trainers.add(trainer1);
        trainers.add(trainer2);
    }

    protected void broadcastMessage(String message) {
        System.out.println(message);
    }

    protected abstract void endFight() ;

    public boolean fightNotOver() {
        return trainers.get(0).isFightable() && trainers.get(1).isFightable();
    }
}
