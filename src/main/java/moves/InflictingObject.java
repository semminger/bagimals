package moves;

import bagimal.Bagimal.Status;
import bagimal.StageStat;
import bagimal.Stat;

public class InflictingObject {
    private InflictingType inflictingType;
    private Status status = null;   // used for statuses
    private StageStat buffStat = null;
    private int buffAmount = 0;
    private int prob = 100;
    private boolean onSelf = true;
    private double hitpointPercentage = 0; //Used for the recoil and healing (negativ for recoil positiv for healing)


    public InflictingObject(Status status, int prob, boolean onSelf) {
        this.inflictingType = InflictingType.STATUS;
        this.status = status;
        this.prob = prob;
        this.onSelf = onSelf;
    }

    public InflictingObject(float hitpointPercentage) {
        this.inflictingType = InflictingType.HEALING_RECOIL;
        this.hitpointPercentage = hitpointPercentage;
    }

    public InflictingObject(StageStat buffStat, int buffAmount, int prob, boolean onSelf) {
        this.inflictingType = InflictingType.STAT_BUFF;
        this.buffStat = buffStat;
        this.buffAmount = buffAmount;
        this.prob = prob;
        this.onSelf = onSelf;
    }

    public InflictingType getInflictingType() {
        return inflictingType;
    }

    public Status getStatus() {
        return status;
    }

    public int getBuffAmount() {
        return buffAmount;
    }

    public StageStat getBuffStat() {
        return buffStat;
    }

    public double getHitpointPercentage() {
        return hitpointPercentage;
    }

    public int getProbability() {
        return prob;
    }

    public boolean onSelf() {
        return onSelf;
    }
}
