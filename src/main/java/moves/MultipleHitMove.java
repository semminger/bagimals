package moves;

import bagimal.Bagimal;
import bagimal.StageStat;
import bagimal.Stat;
import output.OutputController;
import type.Type;
import type.TypeFactor;

public class MultipleHitMove extends Move{

    private int[] hitAmountForProb;

    public MultipleHitMove(int pow, int acc, int pp, Type mov_type, DamageCategory damageCategory, Boolean cannotMiss, String name, String description, InflictingObject infliction, MultipleHitType hitType) {
        super(pow, acc, pp, mov_type, damageCategory, cannotMiss, name, description, infliction);

        switch (hitType) {
            case TWO_FIVE:
                hitAmountForProb = new int[]{2, 2, 3, 3, 4, 5};
                break;
            case DOUBLE:
                hitAmountForProb = new int[] {2};
                break;
        }
    }

    @Override
    public String use(Bagimal user, Bagimal enemy) {

        String output = "";

        if(pp <= 0) {
            output += "Move is out of pp!\n";
            return output;
        }

        output += user.getName() + " uses " + name + "\n";

        // Calculate Accuracy
        int accuracyFactor = 100;
        if(!cannotMiss) {
            int accEvaStage = (user.getBuff(StageStat.ACC) - enemy.getBuff(StageStat.EVA));
            double stageAccFactor = 1.;

            if (accEvaStage > 0) {
                stageAccFactor = (3. + Math.min(Bagimal.MAX_BUF, accEvaStage)) / 3.;
            } else if (accEvaStage < 0) {
                stageAccFactor = 3. / (3. + Math.max(-Bagimal.MAX_BUF, accEvaStage));
            }

            accuracyFactor = (int) (acc * stageAccFactor);
        }
        // Check for hit
        if(ran.nextInt(100) <= accuracyFactor) {

            Stat atkStat = (damageCategory == DamageCategory.PHYSICAL) ? Stat.ATK : Stat.SP_ATK;
            StageStat atkStageStat = (damageCategory == DamageCategory.PHYSICAL) ? StageStat.ATK : StageStat.SP_ATK;
            Stat defStat = (damageCategory == DamageCategory.PHYSICAL) ? Stat.DEF : Stat.SP_DEF;
            StageStat defStageStat = (damageCategory == DamageCategory.PHYSICAL) ? StageStat.DEF : StageStat.SP_DEF;


            // Damage calculation   (Critical Factor is moved to the for loop)
            double lvl = user.getLvl();
            double typeFactor = TypeFactor.getTypeFactor(mov_type, enemy.getType());
            double stabFactor = 1 + (user.getType() == mov_type ? 0.5 : 0);
            double atkBufFactor = 1 + user.getBuff(atkStageStat)/2.0;
            double defBufFactor = 1 + enemy.getBuff(defStageStat)/2.0;
            double randomFactor = (ran.nextInt(25) + 85) / 100.;
            double burnFactor = (user.hasStatus(Bagimal.Status.BURNING) && damageCategory == DamageCategory.PHYSICAL) ? 0.5 : 1;
            double modifier;
            double atk;
            double def;
            int damage;

            int hitCount = getHitCount();
            int hits;

            for(hits = 0; hits < hitCount; hits++) {

                double criticalFactor = (ran.nextInt(512) <= user.getBaseStatValue(Stat.SPD)) ? (2 * lvl) / (lvl + 5) : 1;
                // Check for Critical
                if (criticalFactor > 1) {
                    modifier = criticalFactor * randomFactor * stabFactor * typeFactor;
                    atk = user.getStatValue(atkStat);
                    def = enemy.getStatValue(defStat);
                    output += "WOW, A CRITICAL HIT!!!\n";
                } else {
                    modifier = randomFactor * stabFactor * typeFactor * burnFactor;
                    atk = user.getStatValue(atkStat) * atkBufFactor;
                    def = enemy.getStatValue(defStat) * defBufFactor;
                }

                damage = (int) (((((2 * lvl / 5.) + 2.) * pow * (atk / def) / 50.) + 2.) * modifier);

                enemy.reduceHp(damage);
                if(enemy.isAlive()) {
                    if (infliction != null)
                        output += inflict(infliction, user, enemy, damage);
                } else {
                    break;
                }
            }
            // Hit Prompt
            output += "It hit " + hits + " times!\n";

            // Effectiveness Prompt
            if (typeFactor == 2)
                output += "It's very effective!\n";
            else if (typeFactor == 0.5)
                output += "It's not very effective!\n";

            pp--;
            return output;
        } else {
            output += "It missed!\n";
        }

        pp--;
        return output;
    }

    private int getHitCount() {
        return hitAmountForProb[ran.nextInt(hitAmountForProb.length)];
    }
}
