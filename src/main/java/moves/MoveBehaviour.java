package moves;

public enum MoveBehaviour {
    GENERAL,
    MULTI_HIT,
    CHARGE
}
