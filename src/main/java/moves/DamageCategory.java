package moves;

public enum DamageCategory {
    PHYSICAL,
    SPECIAL,
    STATUS
}
