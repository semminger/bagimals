package moves;

import bagimal.*;
import output.OutputController;
import type.Type;
import type.TypeFactor;

import java.util.Random;

public class Move {

    protected final int pow, acc;
    protected int pp;
    protected final Type mov_type;
    protected final DamageCategory damageCategory;
    protected final String name;
    private final String description;
    protected final InflictingObject infliction;
    protected final boolean cannotMiss;

    protected Random ran;

    public Move(int pow, int acc, int pp, Type mov_type, DamageCategory damageCategory, Boolean cannotMiss, String name, String description, InflictingObject infliction) {
        this.pow = pow;
        this.acc = acc;
        this.pp = pp;
        this.mov_type = mov_type;
        this.damageCategory = damageCategory;
        this.cannotMiss = cannotMiss;
        this.name = name;
        this.infliction = infliction;
        this.description = description;
        ran = new Random();
    }

    // Could also be a status move
    // Return output
    public String use(Bagimal user, Bagimal enemy) {

        String output = "";
        if(pp <= 0) {
            output += "Move is out of pp!\n";
            return output;
        }



        output += user.getName() + " uses " + name + "\n";

        // Calculate Accuracy
        int accuracyFactor = 100;
        if(!cannotMiss) {
            int accEvaStage = (user.getBuff(StageStat.ACC) - enemy.getBuff(StageStat.EVA));
            double stageAccFactor = 1.;

            if (accEvaStage > 0) {
                stageAccFactor = (3. + Math.min(Bagimal.MAX_BUF, accEvaStage)) / 3.;
            } else if (accEvaStage < 0) {
                stageAccFactor = 3. / (3. + Math.max(-Bagimal.MAX_BUF, accEvaStage));
            }
            accuracyFactor = (int) (acc * stageAccFactor);
        }

        // Check for hit
        if(ran.nextInt(100) <= accuracyFactor) {

            int damage = 0;

            if(damageCategory != DamageCategory.STATUS) {
                Stat atkStat = (damageCategory == DamageCategory.PHYSICAL) ? Stat.ATK : Stat.SP_ATK;
                StageStat atkStageStat = (damageCategory == DamageCategory.PHYSICAL) ? StageStat.ATK : StageStat.SP_ATK;
                Stat defStat = (damageCategory == DamageCategory.PHYSICAL) ? Stat.DEF : Stat.SP_DEF;
                StageStat defStageStat = (damageCategory == DamageCategory.PHYSICAL) ? StageStat.DEF : StageStat.SP_DEF;

                // Damage calculation
                double lvl = user.getLvl();
                double typeFactor = TypeFactor.getTypeFactor(mov_type, enemy.getType());
                double stabFactor = 1 + (user.getType() == mov_type ? 0.5 : 0);
                double atkBufFactor = 1 + user.getBuff(atkStageStat) / 2.0;
                double defBufFactor = 1 + enemy.getBuff(defStageStat) / 2.0;
                double criticalFactor = (ran.nextInt(512) <= user.getBaseStatValue(Stat.SPD)) ? (2 * lvl) / (lvl + 5) : 1;
                double randomFactor = (ran.nextInt(16) + 85) / 100.;
                double burnFactor = (user.hasStatus(Bagimal.Status.BURNING) && damageCategory == DamageCategory.PHYSICAL) ? 0.5 : 1;

                double modifier;
                double atk;
                double def;
                if(criticalFactor > 1) {
                    modifier = criticalFactor * randomFactor * stabFactor * typeFactor;
                    atk = user.getStatValue(atkStat);
                    def = enemy.getStatValue(defStat);
                    output += "WOW, A CRITICAL HIT!!!\n";
                } else {
                    modifier = randomFactor * stabFactor * typeFactor * burnFactor;
                    atk = user.getStatValue(atkStat) * atkBufFactor;
                    def = enemy.getStatValue(defStat) * defBufFactor;
                }

                damage = (int) (((((2 * lvl / 5.) + 2.) * pow * (atk / def) / 50.) + 2.) * modifier);

                // Effectiveness Prompt
                if (typeFactor == 2)
                    output += "It's very effective!\n";
                else if (typeFactor == 0.5)
                    output += "It's not very effective!\n";
                else if (typeFactor == 0)
                   output += "It did nothing...\n";
                output += enemy.reduceHp(damage);
            }
            if(infliction != null)
                output += inflict(infliction, user, enemy, damage);

            pp--;
            return output;
        } else {
            output += "It missed!\n";
        }

        pp--;
        return output;
    }

    protected String inflict(InflictingObject infliction, Bagimal user, Bagimal enemy, int damage) {
        String output = "";
        InflictingType inflicType = infliction.getInflictingType();

        switch (inflicType) {
            case STATUS:
                if(ran.nextInt(100) <= infliction.getProbability()) {
                    if (infliction.onSelf()) {
                        output += user.giveStatus(infliction.getStatus());
                    } else {
                        output += enemy.giveStatus(infliction.getStatus());
                    }
                }
                break;

            case STAT_BUFF:
                if(ran.nextInt(100) <= infliction.getProbability()) {
                    if(infliction.onSelf()) {
                        output += user.addStatBuff(infliction.getBuffStat(), infliction.getBuffAmount());
                    } else {
                        output += enemy.addStatBuff(infliction.getBuffStat(), infliction.getBuffAmount());
                    }
                }
                break;

            case HEALING_RECOIL:
                if(infliction.getHitpointPercentage() > 0) {
                    user.regenerateHp((int)(damage * infliction.getHitpointPercentage()));
                } else {
                    output += user.reduceHp((int)(damage * -infliction.getHitpointPercentage())); // Need to multiply it by -1 because damage needs a positive value... I know
                }
        }
        return output;
    }

    public String getName() {
        return name;
    }

    public int getPP() {
        return pp;
    }

    public void reducePP() {
        if(pp > 0)
            pp--;
    }

    public void getInfo() {
        OutputController.write(name+"\n"+description);
        OutputController.write("POW: " + pow);
        OutputController.write("ACC: " + acc);
    }

}
