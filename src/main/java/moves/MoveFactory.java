package moves;

import bagimal.Bagimal.Status;
import bagimal.BagimalFactory;
import bagimal.StageStat;
import bagimal.Stat;
import type.Type;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;

public class MoveFactory {

    public static Move createMove(String moveFileName) {

        //File moveFile = new File("moves/" + moveFileName + ".mv");
        InputStream moveStream = MoveFactory.class.getResourceAsStream("/moves/" + moveFileName + ".mv");
        Scanner fileReader = new Scanner(moveStream);
        MoveBehaviour movBehaviour = MoveBehaviour.GENERAL;
        MultipleHitType hits = MultipleHitType.TWO_FIVE;
        String chargeLine = "";
        int pow = 0, acc = 0, pp = 0;
        Type mov_type = Type.NORMAL;
        DamageCategory dmg_cat = DamageCategory.PHYSICAL;
        boolean cannotMiss = false, isRecharging = false;
        String name = "", desc = "";
        InflictingObject infliction = null;

        while(fileReader.hasNext()) {
            String line = fileReader.nextLine();

            switch (line) {
                case  "[move_beh]":
                    movBehaviour = MoveBehaviour.valueOf(fileReader.nextLine());
                    break;

                case "[hits]":
                    hits = MultipleHitType.valueOf(fileReader.nextLine());
                    break;

                case "[charge_line]":
                    chargeLine = fileReader.nextLine();
                    break;

                case "[isRecharging]":
                    isRecharging = fileReader.hasNextBoolean();
                    break;

                case "[name]":
                    name = fileReader.nextLine();
                    break;

                case "[desc]":
                    desc = fileReader.nextLine();
                    break;

                case "[pow]":
                    pow = fileReader.nextInt();
                    break;

                case "[acc]":
                    acc = fileReader.nextInt();
                    break;

                case "[pp]":
                    pp = fileReader.nextInt();
                    break;

                case "[type]":
                    mov_type = Type.valueOf(fileReader.nextLine());
                    break;

                case "[dmg_cat]":
                    dmg_cat = DamageCategory.valueOf(fileReader.nextLine());
                    break;

                case "[cannotmiss]":
                    cannotMiss = fileReader.hasNextBoolean();
                    break;

                case "[infliction]":
                    String[] inflictionInformation = fileReader.nextLine().split(",");
                    InflictingType inflicType = InflictingType.valueOf(inflictionInformation[0]);

                    switch (inflicType) {
                        case STATUS:
                            infliction = new InflictingObject(Status.valueOf(inflictionInformation[1]),
                                    Integer.parseInt(inflictionInformation[2]),
                                    Boolean.parseBoolean(inflictionInformation[3]));
                            break;

                        case STAT_BUFF:
                            infliction = new InflictingObject(StageStat.valueOf(inflictionInformation[1]),
                                    Integer.parseInt(inflictionInformation[2]),
                                    Integer.parseInt(inflictionInformation[3]),
                                    Boolean.parseBoolean(inflictionInformation[4]));
                            break;

                        case HEALING_RECOIL:
                            infliction = new InflictingObject(Float.parseFloat(inflictionInformation[1]));
                            break;
                    }
            }
        }

        switch (movBehaviour) {

            case GENERAL:
                return new Move(pow, acc, pp, mov_type, dmg_cat, cannotMiss, name, desc, infliction);
            case MULTI_HIT:
                return new MultipleHitMove(pow, acc, pp, mov_type, dmg_cat, cannotMiss, name, desc, infliction, hits);
            case CHARGE:
                return new ChargeMove(pow, acc, pp, mov_type, dmg_cat, cannotMiss, name, desc, infliction, chargeLine, isRecharging);
        }
        return new Move(pow, acc, pp, mov_type, dmg_cat, cannotMiss, name, desc, infliction);
    }
}
