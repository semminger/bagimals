package moves;

public enum InflictingType {
    STATUS,
    STAT_BUFF,
    HEALING_RECOIL,
}
