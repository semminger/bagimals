package moves;

import bagimal.Bagimal;
import output.OutputController;
import type.Type;

public class ChargeMove extends Move{

    private String chargingLine;
    private boolean isRecharging;

    public ChargeMove(int pow, int acc, int pp, Type mov_type, DamageCategory damageCategory, Boolean cannotMiss, String name, String description, InflictingObject infliction, String chargingLine, boolean isRecharging) {
        super(pow, acc, pp, mov_type, damageCategory, cannotMiss, name, description, infliction);
        this.chargingLine = chargingLine;
        this.isRecharging = isRecharging;
    }

    @Override
    public String use(Bagimal user, Bagimal enemy) {
        String output = "";

        if ((user.hasStatus(Bagimal.Status.CHARGING) && !isRecharging) ||
                (isRecharging && !user.hasStatus(Bagimal.Status.RECHARGING))) {
            if(isRecharging) {
                user.giveStatus(Bagimal.Status.RECHARGING);
                user.setChargingMove(this);
            } else {
                user.removeStatus(Bagimal.Status.CHARGING);
                user.removeChargingMove();
            }
            return super.use(user, enemy);
        }
        if((!user.hasStatus(Bagimal.Status.CHARGING) && !isRecharging) ||
                (isRecharging && user.hasStatus(Bagimal.Status.RECHARGING))){
            output += user.getName() + " " + chargingLine + "\n";
            if(isRecharging) {
                user.removeStatus(Bagimal.Status.RECHARGING);
                user.removeChargingMove();
            } else {
                user.giveStatus(Bagimal.Status.CHARGING);
                user.setChargingMove(this);
            }
            return output;
        }
        return  output;
    }
}
