package item;

import bagimal.Bagimal;

public class Antidote extends Item{
    private static final String name = "Antidote";


    public Antidote() {
        super(name);
    }

    @Override
    public void use(Bagimal bagimal) {
        bagimal.removeStatus(Bagimal.Status.POISONED);
    }
}
