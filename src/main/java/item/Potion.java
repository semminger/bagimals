package item;

import bagimal.Bagimal;

public class Potion extends Item {

    private static final String name = "Potion";
    private static final int regHP = 10;

    public Potion() {
        super(name);
    }

    @Override
    public void use(Bagimal bagimal) {
        bagimal.regenerateHp(regHP);
    }
}
