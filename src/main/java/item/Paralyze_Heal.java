package item;

import bagimal.Bagimal;

public class Paralyze_Heal extends Item{

    private static final String name = "Paralyze Healer";

    public Paralyze_Heal() {
        super(name);
    }

    @Override
    public void use(Bagimal bagimal) {
        bagimal.removeStatus(Bagimal.Status.PARALYZED);
    }
}
