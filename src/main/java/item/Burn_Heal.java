package item;

import bagimal.Bagimal;

public class Burn_Heal extends Item{

    private static final String name = "Burn Healer";


    public Burn_Heal() {
        super(name);
    }

    @Override
    public void use(Bagimal bagimal) {
        bagimal.removeStatus(Bagimal.Status.BURNING);
    }
}
