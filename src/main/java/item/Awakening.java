package item;

import bagimal.Bagimal;

public class Awakening extends Item{
    private static final String name = "Awakening";


    public Awakening() {
        super(name);
    }

    @Override
    public void use(Bagimal bagimal) {
        bagimal.removeStatus(Bagimal.Status.SLEEPING);
    }
}
