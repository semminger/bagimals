package item;

import bagimal.Bagimal;

public abstract class Item {

    private String name;

    public Item(String name) {
        this.name = name;
    }

    public abstract void use(Bagimal bagimal);

    //TODO: check if its usable eg. Potion can only be used when the Bagimal is alive

    public String getName() {
        return name;
    }
}
